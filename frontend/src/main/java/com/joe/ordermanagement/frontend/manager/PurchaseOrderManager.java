package com.joe.ordermanagement.frontend.manager;

import com.joe.ordermanagement.frontend.proxy.OrderServiceProxy;
import com.joe.ordermanagement.common.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PurchaseOrderManager {
    @Autowired
    OrderServiceProxy orderServiceProxy;

    public PurchaseOrder submitPurchaseOrder(PurchaseOrder purchaseOrder) {
        return orderServiceProxy.submit(purchaseOrder).getBody();
    }
}
