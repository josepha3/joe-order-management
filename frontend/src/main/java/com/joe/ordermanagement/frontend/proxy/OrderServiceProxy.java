package com.joe.ordermanagement.frontend.proxy;

import com.joe.ordermanagement.frontend.config.ResourceConfig;
import com.joe.ordermanagement.common.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class OrderServiceProxy extends BaseProxy {
    final String orderServiceUrl;

    public OrderServiceProxy(@Autowired ResourceConfig resourceConfig) {
        this.orderServiceUrl = "http://" + resourceConfig.getOrderServiceUrl() + "/order_service";
    }

    public ResponseEntity<PurchaseOrder> submit(PurchaseOrder requestEntity) {
        return restTemplate.postForEntity(orderServiceUrl,
                requestEntity, PurchaseOrder.class);
    }

    public ResponseEntity<PurchaseOrder> get() {
        return restTemplate.getForEntity(orderServiceUrl, PurchaseOrder.class);
    }
}
