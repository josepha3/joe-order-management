package com.joe.ordermanagement.frontend.proxy;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public class BaseProxy {
    @Autowired
    @Getter
    RestTemplate restTemplate;
}
