package com.joe.ordermanagement.frontend.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "order-management.resource")
@Data
public class ResourceConfig {
    private String orderServiceUrl;
}
