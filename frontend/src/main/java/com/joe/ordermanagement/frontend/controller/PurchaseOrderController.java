package com.joe.ordermanagement.frontend.controller;

import com.joe.ordermanagement.frontend.manager.PurchaseOrderManager;
import com.joe.ordermanagement.common.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDateTime;

@Controller
public class PurchaseOrderController {
    @Autowired
    PurchaseOrderManager purchaseOrderManager;

    @GetMapping("purchase-order")
    public String createPurchaseOrder(Model model) {
        model.addAttribute(PurchaseOrder.builder()
                .creationDate(LocalDateTime.now())
                .build()
        );
        return "create-purchase-order";
    }

    @PostMapping("purchase-order")
    public String submitPurchaseOrder(@ModelAttribute PurchaseOrder purchaseOrder, Model model) {
        model.addAttribute(purchaseOrderManager.submitPurchaseOrder(purchaseOrder));
        return "submit-purchase-order-success";
    }
}
