package com.joe.ordermanagement.orderservice.controller;

import com.joe.ordermanagement.common.model.PurchaseOrder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("order_service")
public class OrderServiceController {

    @ResponseBody
    @PostMapping
    public PurchaseOrder submitPurchaseOrder(@RequestBody PurchaseOrder purchaseOrder) {
        return PurchaseOrder.builder()
                .purchaseOrderId("222")
                .build();
    }
}
