package com.joe.ordermanagement.common.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PurchaseOrder {
    String purchaseOrderId;
    LocalDateTime creationDate;
    List<ProductPackage> productPackages;
}
